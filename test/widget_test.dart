import 'package:dokitest/domain/entities/user.dart';
import 'package:dokitest/domain/usecase/auth/login_usecase.dart';
import 'package:dokitest/domain/usecase/auth/user_usecase.dart';
import 'package:flutter_test/flutter_test.dart';

import 'repositories/mock_auth_repository.dart';
import 'repositories/mock_user_repository.dart';

void main() {
  test("Login Success test", () async {
    const param = LoginAuthParam(userName: "amir", password: "12345678");
    final useCase = LoginUseCase(repository: MockAuthRepository());
    bool isSuccess = await useCase.call(param);
    expect(isSuccess, true);
  });

  test("Login Failed test", () async {
    const param = LoginAuthParam(userName: "amir", password: "1234");
    final useCase = LoginUseCase(repository: MockAuthRepository());
    bool isSuccess = await useCase.call(param);
    expect(isSuccess, false);
  });

  test("get users home", () async {
    final useCase = UserUseCase(repository: MockUserRepository());
    final List<User> users = await useCase.call();
    expect(users, isNotNull);
  });
}
