import 'dart:convert';
import 'dart:io';

import 'package:dokitest/data/mapper/user_mapper.dart';
import 'package:dokitest/data/models/user_model.dart';
import 'package:dokitest/domain/entities/user.dart';
import 'package:dokitest/domain/repositories/user_repository.dart';

class MockUserRepository extends UserRepository{
  @override
  Future<List<User>> getUsers() async {
    final file = File('test/data/profile.json');
    final response = await file.readAsString();
    final data = await json.decode(response);

    final users = List<UserModel>.from(data.map((dynamic json) {
      return UserModel.fromJson(json);
    }));

    final List<User> usersModel = users.map((e) => UserMapper.toEntity(e)).toList();

    return usersModel;
  }
}