import 'package:dokitest/domain/repositories/auth_repository.dart';
import 'package:dokitest/domain/usecase/auth/login_usecase.dart';

class MockAuthRepository extends AuthRepository{
  @override
  Future<bool> login({required LoginAuthParam param}) async{
    await Future.delayed(Duration(seconds: 1));
    return param.userName == "amir" && param.password=="12345678";
  }
}