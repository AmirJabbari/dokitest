import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';

@freezed
class User extends Equatable with _$User {
  const User._();

  factory User({
    required String guid,
    required bool? isOwner,
    required String? balance,
    required String? picture,
    required int? age,
    required String? eyeColor,
    required String? name,
    required String? gender,
    required String? company,
    required String? email,
    required String? phone,
    required String? address,
    required String? about,
    required String? greeting,
    required String? favoriteFruit,
    required List<User>? friends,
  }) = _User;

  @override
  List<Object?> get props => [];
}
