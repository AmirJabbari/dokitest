
import 'package:dokitest/domain/usecase/auth/login_usecase.dart';

abstract class AuthRepository {
  Future<bool> login({
    required LoginAuthParam param
  });
}
