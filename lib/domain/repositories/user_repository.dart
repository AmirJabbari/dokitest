import 'package:dokitest/core/repository/base_repository.dart';
import 'package:dokitest/domain/entities/user.dart';

abstract class UserRepository extends BaseRepository {
  Future<List<User>> getUsers();
}
