import 'package:dokitest/core/usecase/param_usecase.dart';
import 'package:dokitest/domain/repositories/auth_repository.dart';

class LoginUseCase extends ParamUseCase<bool, LoginAuthParam> {
  final AuthRepository repository;

  LoginUseCase({
    required this.repository,
  });

  @override
  Future<bool> call(LoginAuthParam params) {
    return repository.login(param: params);
  }
}

class LoginAuthParam {
  final String userName;
  final String password;

  const LoginAuthParam({
    required this.userName,
    required this.password,
  });
}
