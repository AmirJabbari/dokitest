import 'package:dokitest/core/usecase/no_param_usecase.dart';
import 'package:dokitest/domain/entities/user.dart';
import 'package:dokitest/domain/repositories/user_repository.dart';

class UserUseCase extends NoParamUseCase<List<User>> {
  final UserRepository repository;

  UserUseCase({
    required this.repository,
  });

  @override
  Future<List<User>> call() {
    return repository.getUsers();
  }
}
