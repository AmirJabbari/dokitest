import 'package:dokitest/presentation/utils/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../presentation/utils/constants/assets.dart';

ThemeData get lightTheme {
  return ThemeData.light().copyWith(
    brightness: Brightness.light,
    primaryColor: PrimaryColor.light,
    dividerColor: Colors.transparent,
    scaffoldBackgroundColor: BackgroundColor.light,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    textSelectionTheme: TextSelectionThemeData(
      cursorColor: TextPrimaryColor.light,
      selectionColor: PrimaryColor.light,
      selectionHandleColor: PrimaryColor.light,
    ),
    bottomSheetTheme: BottomSheetThemeData(
      backgroundColor: BackgroundColor.light,
    ),
    appBarTheme: AppBarTheme(
      systemOverlayStyle: SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: BackgroundColor.light,
        statusBarBrightness: Brightness.light,
      ),
      elevation: 0,
      backgroundColor: BackgroundColor.light,
    ),
    colorScheme: ColorScheme.light(
        brightness: Brightness.light,
        primary: PrimaryColor.light,
        error: DangerColor.light,
        background: BackgroundColor.light,
        onBackground: BackgroundSecondaryColor.light,
        onSecondary: BackgroundTertiaryColor.light,
        tertiary: TextPrimaryColor.light,
        onTertiary: TextSubtitleColor.light,
        tertiaryContainer: DisabledTextColor.light,
        onTertiaryContainer: TextButtonColor.light,
        surfaceTint: DisabledColor.light,
        outline: BorderColor.light,
        surface: YellowColor.light,
        onSurface: BlueColor.light,
        surfaceVariant: GreenColor.light,
        onSurfaceVariant: ActiveBorderColor.light,
        inverseSurface: HoverColor.light,
        onPrimary: SecondaryRed.light,
        shadow: ShadowColor.light,
        onPrimaryContainer: IconColor.light),
    textTheme: TextTheme(
      headlineLarge: TextStyle(
        fontSize: 28,
        fontFamily: Assets.fontName,
        color:  TextPrimaryColor.light,
        fontWeight: FontWeight.w700,
        height: 1.4,
        fontFamilyFallback: const [Assets.fontName],
      ),
      headlineMedium: TextStyle(
          fontSize: 24,
          fontFamily: Assets.fontName,
          color:  TextPrimaryColor.light,
          fontWeight: FontWeight.w700,
          height: 1.5,
          fontFamilyFallback: const [Assets.fontName]),
      headlineSmall: TextStyle(
          fontSize: 20,
          fontFamily: Assets.fontName,
          color:  TextPrimaryColor.light,
          fontWeight: FontWeight.w700,
          height: 1.6,
          fontFamilyFallback: const [Assets.fontName]),
      labelLarge: TextStyle(
          fontSize: 18,
          fontFamily: Assets.fontName,
          color:  TextPrimaryColor.light,
          fontWeight: FontWeight.w500,
          height: 1.77,
          fontFamilyFallback: const [Assets.fontName]),
      labelMedium: TextStyle(
          fontSize: 16,
          fontFamily: Assets.fontName,
          color:  TextPrimaryColor.light,
          fontWeight: FontWeight.w500,
          height: 1.75,
          fontFamilyFallback: const [Assets.fontName]),
      labelSmall: TextStyle(
          fontSize: 14,
          fontFamily: Assets.fontName,
          color:  TextPrimaryColor.light,
          fontWeight: FontWeight.w500,
          height: 1.7,
          fontFamilyFallback: const [Assets.fontName]),
      bodyLarge: TextStyle(
          fontSize: 18,
          fontFamily: Assets.fontName,
          color:  TextPrimaryColor.light,
          fontWeight: FontWeight.w400,
          height: 1.77,
          fontFamilyFallback: const [Assets.fontName]),
      bodyMedium: TextStyle(
          fontSize: 16,
          fontFamily: Assets.fontName,
          color:  TextPrimaryColor.light,
          fontWeight: FontWeight.w400,
          height: 1.75,
          fontFamilyFallback: const [Assets.fontName]),
      bodySmall: TextStyle(
          fontSize: 14,
          fontFamily: Assets.fontName,
          color:  TextPrimaryColor.light,
          fontWeight: FontWeight.w400,
          height: 1.7,
          fontFamilyFallback: const [Assets.fontName]),
    ),
  );
}
