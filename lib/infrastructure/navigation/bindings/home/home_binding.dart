import 'package:dokitest/data/repositories/user_repository_impl.dart';
import 'package:dokitest/domain/usecase/auth/user_usecase.dart';
import 'package:dokitest/presentation/pages/home/controllers/home_controller.dart';
import 'package:get/get.dart';


class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(
      () => HomeController(useCase: UserUseCase(repository: Get.find<UserRepositoryImpl>())),
    );
  }
}
