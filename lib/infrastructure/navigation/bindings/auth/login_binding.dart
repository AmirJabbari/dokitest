import 'package:dokitest/data/repositories/auth_repository_impl.dart';
import 'package:dokitest/domain/usecase/auth/login_usecase.dart';
import 'package:dokitest/presentation/pages/auth/controllers/login_controller.dart';
import 'package:get/get.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LoginController>(
      () => LoginController(
        loginUseCase: LoginUseCase(
          repository: Get.find<AuthRepositoryIml>(),
        ),
      ),
    );
  }
}
