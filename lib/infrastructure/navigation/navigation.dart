import 'package:dokitest/infrastructure/navigation/bindings/auth/login_binding.dart';
import 'package:dokitest/infrastructure/navigation/bindings/home/home_binding.dart';
import 'package:dokitest/infrastructure/navigation/bindings/profile/profile_binding.dart';
import 'package:dokitest/presentation/pages/auth/login_screen.dart';
import 'package:dokitest/presentation/pages/home/home_screen.dart';
import 'package:dokitest/presentation/pages/profile/profile_screen.dart';
import 'package:get/get.dart';

part 'routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.LOGIN;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeScreen(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => const LoginPage(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.PROFILE,
      page: () => ProfileScreen(),
      binding: ProfileBinding(),
    )
  ];
}
