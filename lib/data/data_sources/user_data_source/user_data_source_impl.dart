import 'dart:convert';

import 'package:dokitest/data/data_sources/user_data_source/user_data_source.dart';
import 'package:dokitest/data/models/user_model.dart';
import 'package:flutter/services.dart';

class UserDataSourceImpl  extends UserDataSource{
  @override
  Future<List<UserModel>> getUsers() async{

    await Future.delayed(const Duration(seconds: 3));

    try {
      // Load the JSON file from assets
      final String jsonString = await rootBundle.loadString('assets/userPrfofile.json');

      // Parse the JSON data
      final List<dynamic> jsonList = json.decode(jsonString);

      // Map the JSON data to UserModel objects
      final List<UserModel> users = jsonList.map((dynamic json) {
        return UserModel.fromJson(json); // Assuming you have a UserModel.fromJson constructor
      }).toList();

      return users;
    } catch (e) {
      // Handle any errors (e.g., file not found, JSON parsing error)
      print('Error loading and parsing JSON: $e');
      return [];
    }


  }

}