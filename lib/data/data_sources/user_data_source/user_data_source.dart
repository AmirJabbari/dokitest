import 'package:dokitest/data/models/user_model.dart';

abstract class UserDataSource{
  Future<List<UserModel>> getUsers();
}