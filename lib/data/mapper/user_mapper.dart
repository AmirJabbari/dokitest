import 'package:dokitest/data/models/user_model.dart';
import 'package:dokitest/domain/entities/user.dart';

class UserMapper {
  const UserMapper._();

  static User toEntity(UserModel user) {
    return User(
        guid: user.guid,
        isOwner: user.isOwner,
        balance: user.balance,
        picture: user.picture,
        age: user.age,
        eyeColor: user.eyeColor,
        name: user.name,
        gender: user.gender,
        company: user.company,
        email: user.email,
        phone: user.phone,
        address: user.address,
        about: user.about,
        greeting: user.greeting,
        favoriteFruit: user.favoriteFruit,
        friends: user.friends?.map((e) => toEntity(e)).toList());
  }

  static UserModel toModel(User user) {
    return UserModel(
      guid: user.guid,
      isOwner: user.isOwner,
      balance: user.balance,
      picture: user.picture,
      age: user.age,
      eyeColor: user.eyeColor,
      name: user.name,
      gender: user.gender,
      company: user.company,
      email: user.email,
      phone: user.phone,
      address: user.address,
      about: user.about,
      greeting: user.greeting,
      favoriteFruit: user.favoriteFruit,
      friends: [],
    );
  }
}
