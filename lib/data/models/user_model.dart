import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
class UserModel {
  @JsonKey(name: 'guid')
  final String guid;
  @JsonKey(name: 'isOwner')
  final bool? isOwner;
  @JsonKey(name: 'balance')
  final String? balance;
  @JsonKey(name: 'picture')
  final String? picture;
  @JsonKey(name: 'age')
  final int? age;
  @JsonKey(name: 'eyeColor')
  final String? eyeColor;
  @JsonKey(name: 'name')
  final String? name;
  @JsonKey(name: 'gender')
  final String? gender;
  @JsonKey(name: 'company')
  final String? company;
  @JsonKey(name: 'email')
  final String? email;
  @JsonKey(name: 'phone')
  final String? phone;
  @JsonKey(name: 'address')
  final String? address;
  @JsonKey(name: 'about')
  final String? about;
  @JsonKey(name: 'greeting')
  final String? greeting;
  @JsonKey(name: 'favoriteFruit')
  final String? favoriteFruit;
  @JsonKey(name: 'friends')
  final List<UserModel>? friends;
  UserModel({
    required this.guid,
    required this.isOwner,
    required this.balance,
    required this.picture,
    required this.age,
    required this.eyeColor,
    required this.name,
    required this.gender,
    required this.company,
    required this.email,
    required this.phone,
    required this.address,
    required this.about,
    required this.greeting,
    required this.favoriteFruit,
    required this.friends,
  });


factory UserModel.fromJson(Map<String, dynamic> json) =>
    _$UserModelFromJson(json);

Map<String, dynamic> toJson() => _$UserModelToJson(this);
}
