import 'package:dokitest/data/data_sources/user_data_source/user_data_source.dart';
import 'package:dokitest/data/mapper/user_mapper.dart';
import 'package:dokitest/domain/entities/user.dart';
import 'package:dokitest/domain/repositories/user_repository.dart';

class UserRepositoryImpl extends UserRepository {
  final UserDataSource userDataSource;

  UserRepositoryImpl({
    required this.userDataSource,
  });

  @override
  Future<List<User>> getUsers() async {
    return safeCall(
      method: userDataSource.getUsers(),
      mapper: (users) => users.map((e) => UserMapper.toEntity(e)).toList(),
    );
  }
}
