import 'package:dokitest/domain/usecase/auth/login_usecase.dart';
import 'package:dokitest/infrastructure/navigation/navigation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  LoginController({
    required this.loginUseCase,
  });

  final LoginUseCase loginUseCase;
  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  RxString requestError = "".obs;
  RxString? userNameError = "".obs;
  RxString? passwordError = "".obs;
  RxBool loading = false.obs;
  RxBool isEnableForm = false.obs;

  bool get validateUserName {
    if (userNameController.text.length >= 3) {
      userNameError = "".obs;
      return true;
    } else {
      userNameError?.value = "login_username_error".tr;
      return false;
    }
  }

  bool get validatePassword {
    if (passwordController.text.length >= 8) {
      passwordError = "".obs;
      return true;
    } else {
      passwordError?.value = "login_password_error".tr;
      return false;
    }
  }
  void checkForm(){
    if(userNameController.text.length>=3 && passwordController.text.length>=8){
      isEnableForm.value=true;
    }else{
      isEnableForm.value=false;
    }
    update();
  }

  Future<void> login() async {
    loading.value = true;
    isEnableForm.value=false;
    update();
    var isSuccess = await loginUseCase.call(
      LoginAuthParam(
        userName: userNameController.text,
        password: passwordController.text,
      ),
    );
    loading.value = false;
    isEnableForm.value=true;

    update();
    if (isSuccess) {
      Get.offAndToNamed(Routes.HOME);
    } else {
      Get.showSnackbar(const GetSnackBar(
        message: "username = amir password = 12345678",
        backgroundColor: Colors.red,
        duration: Duration(seconds: 2),
      ));
    }
  }
}
