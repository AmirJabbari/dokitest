import 'package:dokitest/core/utils/extensions/context_entension.dart';
import 'package:dokitest/presentation/common_widgets/abstract/base_view.dart';
import 'package:dokitest/presentation/common_widgets/widgets/button_widget.dart';
import 'package:dokitest/presentation/common_widgets/widgets/text_field_widget.dart';
import 'package:dokitest/presentation/common_widgets/widgets/text_widget.dart';
import 'package:dokitest/presentation/pages/auth/controllers/login_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginPage extends BaseView<LoginController> {
  const LoginPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget body(BuildContext context) {
    return GetBuilder<LoginController>(builder: (controller) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextWidget.bold(
            "login_login".tr,
            context: context,
            additionalStyle: const TextStyle(fontSize: 16),
          ),
          const SizedBox(height: 16),
          CustomTextField(
            hintText: "login_username".tr,
            controller: controller.userNameController,
            errorText: (controller.userNameError?.value.isNotEmpty ?? true)
                ? controller.userNameError?.value
                : null,
            onFieldSubmitted: (value) {
              controller.validateUserName;
              controller.update();
            },
            onChanged: (value) {
              controller.validatePassword;
              controller.checkForm();
            },
            hintStyle: TextStyle(
              color: context.colorScheme.onTertiary,
            ),
          ),
          const SizedBox(height: 8),
          CustomTextField(
            hintText: "login_password".tr,
            controller: controller.passwordController,
            errorText: (controller.passwordError?.value.isNotEmpty ?? true)
                ? controller.passwordError?.value
                : null,
            isObscure: true,
            onFieldSubmitted: (value) {
              controller.validatePassword;
              controller.update();
            },
            onChanged: (value) {
              controller.validatePassword;
              controller.checkForm();
            },
            hintStyle: TextStyle(
              color: context.colorScheme.onTertiary,
            ),
          ),
          const SizedBox(height: 16),
          ButtonWidget.fill(
            context: context,
            width: context.screenWidth,
            text: "login_login".tr,
            isEnable: controller.isEnableForm.value,
            loadingStatus: controller.loading.value
                ? ButtonLoadingStatus.loading
                : ButtonLoadingStatus.normal,
            onPressed: () {
              controller.login();
            },
          )
        ],
      );
    });
  }
}
