import 'package:dokitest/domain/entities/user.dart';
import 'package:dokitest/domain/usecase/auth/user_usecase.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  final UserUseCase useCase;
  RxBool loading = true.obs;
  var users = RxList<User>([]);

  HomeController({
    required this.useCase,
  });

  @override
  void onInit() async {
    var response = await useCase.call();
    users.value = response;
    loading.value = false;
    update();
    super.onInit();
  }
}
