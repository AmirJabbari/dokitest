import 'package:dokitest/core/utils/extensions/context_entension.dart';
import 'package:dokitest/core/utils/extensions/int_extensions.dart';
import 'package:dokitest/presentation/common_widgets/widgets/skeleton_widget.dart';
import 'package:flutter/material.dart';

class HomeSkeletonLoading extends StatelessWidget {
  const HomeSkeletonLoading({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemBuilder: (context, index) => Container(
        height: 86,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: context.colorScheme.onBackground,
        ),
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child:const Row(
          children: [
            SkeletonWidget.circular(
              width: 42,
              height: 42,
            ),
             SizedBox(width: 8),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SkeletonWidget.rectangular(
                  width: 62,
                  height: 14,
                ),
                SizedBox(height: 8),
                SkeletonWidget.rectangular(
                  width: 100,
                  height: 14,
                ),
                SizedBox(height: 9),

                SkeletonWidget.rectangular(
                  width: 250,
                  height: 14,
                ),
              ],
            )
          ],
        ),
      ),
      separatorBuilder: (context, index) => const SizedBox(height: 8),
      itemCount: 12.generateRandom(min: 4),
    );
  }
}
