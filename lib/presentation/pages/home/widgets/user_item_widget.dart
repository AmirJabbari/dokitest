import 'package:dokitest/core/utils/extensions/context_entension.dart';
import 'package:dokitest/domain/entities/user.dart';
import 'package:dokitest/presentation/common_widgets/widgets/avatar_widget.dart';
import 'package:dokitest/presentation/common_widgets/widgets/text_widget.dart';
import 'package:flutter/material.dart';

class UserItemWidget extends StatelessWidget {
  final User user;

  const UserItemWidget({super.key, required this.user});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 86,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: context.colorScheme.onBackground,
      ),
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Row(
        children: [
          AvatarWidget(
            imageUrl: user.picture ?? "",
            name: user.name ?? "",
            width: 42,
            height: 42,
          ),
          const SizedBox(width: 8),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextWidget.bold(
                user.name ?? "",
                context: context,
                additionalStyle: const TextStyle(
                  fontSize: 14,
                ),
              ),
              TextWidget.regular(
                user.email ?? "",
                context: context,
                additionalStyle: const TextStyle(
                  fontSize: 12,
                ),
              ),
              TextWidget.regular(
                "${user.address}",
                context: context,
                additionalStyle: TextStyle(
                    fontSize: 10, color: context.colorScheme.onTertiary),
              )
            ],
          )
        ],
      ),
    );
  }
}
