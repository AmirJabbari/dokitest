import 'package:dokitest/presentation/common_widgets/widgets/avatar_widget.dart';
import 'package:dokitest/presentation/common_widgets/widgets/text_widget.dart';
import 'package:flutter/material.dart';

class RowProfileHeader extends StatelessWidget {
  final String? title;
  final String? userName;
  final String? userAvatar;
  final Widget? action;
  final bool hasName;
  final double? height;
  final double? width;
  final VoidCallback? onTapProfile;

  const RowProfileHeader({
    Key? key,
    this.title,
    this.userAvatar,
    this.userName,
    this.action,
    this.hasName = true,
    this.height,
    this.width,
    this.onTapProfile,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Row(
          children: [
            InkWell(
              onTap: onTapProfile,
              child: Row(
                children: [
                  AvatarWidget(
                    imageUrl: userAvatar??"",
                    width: width ?? 50,
                    height:  50,
                    boxShape: BoxShape.circle,
                    // boxFit: BoxFit.fill,
                    name: userName??"",
                  ),
                  const SizedBox(width: 12),
                  Visibility(
                    visible: hasName,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextWidget.bold(
                          userName??"",
                          context: context,
                          additionalStyle: const TextStyle(
                            fontSize: 18,
                          ),
                        ),

                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        TextWidget.bold(
          title ?? "",
          context: context,
          additionalStyle: const TextStyle(fontSize: 18),
        ),
      ],
    );
  }
}
