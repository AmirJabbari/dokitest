import 'package:dokitest/infrastructure/navigation/navigation.dart';
import 'package:dokitest/presentation/common_widgets/abstract/base_view.dart';
import 'package:dokitest/presentation/pages/home/widgets/home_skeleton_loading.dart';
import 'package:dokitest/presentation/pages/home/widgets/row_profile_header.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controllers/home_controller.dart';
import 'widgets/user_item_widget.dart';

class HomeScreen extends BaseView<HomeController> {
  const HomeScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget body(BuildContext context) {
    return GetBuilder<HomeController>(builder: (controller) {
      return Column(
        children: [
          RowProfileHeader(
            userName: "Amir Jabbari",
            userAvatar: "https://lh3.googleusercontent.com/a/ACg8ocI0FCVzHPu3CNk92nJDxTMqWhzi1Z9srtoiMg3AnZ3JFw=s360-c-no",
            onTapProfile: () {
              Get.toNamed(Routes.PROFILE, arguments: [
                controller.users[0],
                controller.users,
              ]);
            },
          ),
          const SizedBox(height: 16),
          Expanded(
            child: Visibility(
              visible: !(controller.loading.value),
              replacement: const HomeSkeletonLoading(),
              child: ListView.separated(
                physics: const BouncingScrollPhysics(),
                itemCount: controller.users.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return UserItemWidget(
                    user: controller.users[index],
                  );
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(height: 8),
              ),
            ),
          )
        ],
      );
    });
  }
}
