import 'package:dokitest/core/utils/extensions/context_entension.dart';
import 'package:dokitest/domain/entities/user.dart';
import 'package:dokitest/presentation/common_widgets/widgets/text_widget.dart';
import 'package:flutter/material.dart';

class FriendItemWidget extends StatelessWidget {
  final User user;
  final Function(String guid) onTap;

  const FriendItemWidget({
    super.key,
    required this.user,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        onTap.call(user.guid);
      },
      child: Container(
        width: 100,
        height: 50,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            color: context.colorScheme.onBackground),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextWidget.bold(
              user.name ?? "",
              context: context,
              additionalStyle: const TextStyle(fontSize: 12),
            )
          ],
        ),
      ),
    );
  }
}
