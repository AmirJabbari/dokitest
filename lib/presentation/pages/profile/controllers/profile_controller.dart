import 'package:dokitest/domain/entities/user.dart';
import 'package:dokitest/infrastructure/navigation/navigation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProfileController extends GetxController {
  var argument = Get.arguments;
  User? user;
  List<User>? users;

  @override
  void onInit() {
    user = argument[0];
    users = argument[1];
    super.onInit();
  }

  void onTapUser({required String guid, required User user}) {
    User? foundedUser = users?.firstWhereOrNull((element) => element.guid==guid);
    if (foundedUser !=null) {
      Get.delete<ProfileController>();
      Get.offAndToNamed(Routes.PROFILE, arguments: [
        foundedUser,
        users,
      ]);
    }else{
      Get.showSnackbar(const GetSnackBar(
        message: "user not found!!",
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ));
    }
  }

}
