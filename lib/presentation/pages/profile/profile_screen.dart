import 'package:dokitest/core/utils/extensions/context_entension.dart';
import 'package:dokitest/presentation/common_widgets/abstract/base_view.dart';
import 'package:dokitest/presentation/common_widgets/widgets/button_widget.dart';
import 'package:dokitest/presentation/common_widgets/widgets/text_widget.dart';
import 'package:dokitest/presentation/pages/home/widgets/row_profile_header.dart';
import 'package:dokitest/presentation/pages/profile/controllers/profile_controller.dart';
import 'package:dokitest/presentation/pages/profile/widgets/friend_item_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class ProfileScreen extends BaseView<ProfileController> {
  @override
  Widget body(BuildContext context) {
    return GetBuilder<ProfileController>(
      builder: (controller) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RowProfileHeader(
              userName: controller.user?.name ?? "",
              userAvatar: controller.user?.picture ?? "",
            ),
            const SizedBox(height: 16),
            Container(
              height: 130,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                color: context.colorScheme.onBackground,
              ),
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              child: Column(
                children: [
                  Row(
                    children: [
                      TextWidget.bold(
                        "Balance: ",
                        context: context,
                        additionalStyle: TextStyle(
                            fontSize: 14,
                            color: context.colorScheme.onTertiary),
                      ),
                      const SizedBox(width: 8),
                      Expanded(
                        child: TextWidget.bold(
                          controller.user?.balance ?? "",
                          context: context,
                          additionalStyle: const TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ),
                      Visibility(
                        visible: controller.user?.isOwner ?? false,
                        child: ButtonWidget.outline(
                          context: context,
                          height: 36,
                          width: 100,
                          text: "Edit",
                        ),
                      )
                    ],
                  ),
                  const SizedBox(height: 8),
                  Row(
                    children: [
                      TextWidget.bold(
                        "Age: ",
                        context: context,
                        additionalStyle: TextStyle(
                            fontSize: 14,
                            color: context.colorScheme.onTertiary),
                      ),
                      const SizedBox(width: 8),
                      TextWidget.bold(
                        controller.user!.age.toString(),
                        context: context,
                        additionalStyle: const TextStyle(
                          fontSize: 14,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 8),
                  Row(
                    children: [
                      TextWidget.bold(
                        "About: ",
                        context: context,
                        additionalStyle: TextStyle(
                            fontSize: 14,
                            color: context.colorScheme.onTertiary),
                      ),
                      const SizedBox(width: 8),
                      Expanded(
                        child: TextWidget.bold(
                          controller.user?.about ?? "",
                          context: context,
                          overflow: TextOverflow.ellipsis,
                          additionalStyle: const TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 32),
            TextWidget.bold(
              "Friends",
              context: context,
              additionalStyle: const TextStyle(fontSize: 16),
            ),
            const SizedBox(height: 16),
            SizedBox(
              height: 60,
              child: Visibility(
                visible: controller.user!.friends?.isNotEmpty ?? false,
                replacement: SizedBox(
                  height: 100,
                  child: Center(
                    child: TextWidget.semiBold(
                      "You have no friends",
                      context: context,
                      additionalStyle: const TextStyle(
                        fontSize: 12,
                      ),
                    ),
                  ),
                ),
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemBuilder: (context, index) =>
                      FriendItemWidget(
                        user: controller.user!.friends![index],
                        onTap: (String guid) {
                          controller.onTapUser(
                              guid: guid, user: controller.user!
                              .friends![index]);
                        },
                      ),
                  separatorBuilder: (context, index) =>
                  const SizedBox(width: 8),
                  itemCount: controller.user?.friends?.length ?? 0,
                ),
              ),
            ),
            const SizedBox(height: 16),
            TextWidget.bold(
              "Greeting",
              context: context,
              additionalStyle: const TextStyle(fontSize: 16),
            ),
            const SizedBox(height: 16),
            Container(
              width: context.screenWidth,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: context.colorScheme.onBackground
              ),
              padding: const EdgeInsets.all(16),
              child: TextWidget.medium(
                controller.user?.greeting ?? "", context: context,
                textAlign: TextAlign.center,
                additionalStyle: const TextStyle(fontSize: 12),),
            )
          ],
        );
      },
    );
  }
}
