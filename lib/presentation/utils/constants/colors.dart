import 'package:flutter/material.dart';

class PrimaryColor{
  static Color get light => const Color(0xffE6294D);

  static Color get dark => const Color(0xffE6294D);
}

class TextPrimaryColor{
  static Color get light => const Color(0xff353535);

  static Color get dark => const Color(0xffFFFFFF);
}

class TextButtonColor{
  static Color get light => const Color(0xffFFFFFF);

  static Color get dark => const Color(0xffFFFFFF);
}

class TextSubtitleColor{
  static Color get light => const Color(0xff818181);

  static Color get dark => const Color(0xffBBBBBB);
}

class DisabledTextColor{
  static Color get light => const Color(0xffBDBEBF);

  static Color get dark => const Color(0xff777777);
}

class BackgroundColor{
  static Color get light => const Color(0xffFAFAFA);

  static Color get dark => const Color(0xff0F1922);
}

class BackgroundSecondaryColor{
  static Color get light => const Color(0xffFFFFFF);

  static Color get dark => const Color(0xff243443);
}

class BackgroundTertiaryColor{
  static Color get light => const Color(0xffF6F6F6);

  static Color get dark => const Color(0xff0D1720);
}

class YellowColor{
  static Color get light => const Color(0xffFFCC00);

  static Color get dark => const Color(0xffFFE680);
}

class BlueColor{
  static Color get light => const Color(0xff00C7BE);

  static Color get dark => const Color(0xff80E3DF);
}

class GreenColor{
  static Color get light => const Color(0xff34C759);

  static Color get dark => const Color(0xff9AE3AC);
}

class BorderColor{
  static Color get light => const Color(0xffEDEDED);

  static Color get dark => const Color(0xff324353);
}

class DisabledColor{
  static Color get light => const Color(0xffE9E9EA);

  static Color get dark => const Color(0xff464C51);
}
class ActiveBorderColor{
  static Color get light => const Color(0xff4E4E4E);

  static Color get dark => const Color(0xffFFFFFF);
}

class HoverColor{
  static Color get light => const Color(0xffFF6C88);

  static Color get dark => const Color(0xffFF6C88);
}

class DangerColor{
  static Color get light => const Color(0xffFF3B30);

  static Color get dark => const Color(0xffFF9D98);
}
class ShadowColor{
  static Color get light => const Color(0xff000000);

  static Color get dark => const Color(0xff020C15);
}

class IconColor{
  static Color get light => const Color(0xff292D32);

  static Color get dark => const Color(0xffFFFFFF);
}
class SecondaryRed{
  static Color get light => const Color(0xffFFEBEA);

  static Color get dark => const Color(0xffFFEBEA);
}

class ShimmerBaseColor {
  static Color get light => const Color(0xffF5F5F5);

  static Color get dark => const Color(0xff14212B);
}

class ShimmerHighlightColor {
  static Color get light => const Color(0xffF3F3F3);

  static Color get dark => const Color(0xff0D1720);
}
