import 'package:dokitest/core/utils/extensions/context_entension.dart';
import 'package:flutter/material.dart';

class Loading extends StatelessWidget {
  final double? width;
  final double? height;
  final Color? color;

  const Loading({
    Key? key,
    this.width,
    this.height,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height ?? 24,
      width: width ?? 24,
      child: CircularProgressIndicator(
        color: color ?? context.colorScheme.primary,
        strokeWidth: 3,
      ),
    );
  }
}