import 'package:flutter/material.dart';
import 'package:intl/intl.dart' show toBeginningOfSentenceCase;

import 'text_widget.dart';

class NoAvatarPlaceholderWidget extends StatelessWidget {
  final String? name;
  final bool colorize;
  final double? fontSize;
  final Color? color;
  final double? width, height;

  const NoAvatarPlaceholderWidget(
      {Key? key,
        required this.name,
        required this.colorize,
        this.fontSize,
        this.color,
        this.width,
        this.height})
      : super(key: key);

  const NoAvatarPlaceholderWidget.colorful({
    Key? key,
    required this.name,
    this.fontSize,
    this.colorize = true,
    this.color,
    this.width,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int characterIndex = englishAlphabet
        .indexWhere((element) => element == name?[0]);
    Color randomColor = Colors.primaries[0];
    if (characterIndex != -1) {
      randomColor = Colors.primaries[characterIndex % Colors.primaries.length];
    }

    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: colorize ? color ?? randomColor : null,
        shape: BoxShape.circle,
      ),
      padding: EdgeInsets.zero,
      margin: EdgeInsets.zero,
      child: Center(
        child: TextWidget.medium(
          toBeginningOfSentenceCase(name) ?? '',
          context: context,
          softWrap: false,
          textAlign: TextAlign.center,
          additionalStyle: TextStyle(
            color: Colors.white,
            fontSize: fontSize,
          ),
        ),
      ),
    );
  }

  static const List<String> englishAlphabet = [
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
  ];

}
