import 'package:cached_network_image/cached_network_image.dart';
import 'package:dokitest/presentation/common_widgets/widgets/no_avatar_place_holder_widget.dart';
import 'package:dokitest/presentation/common_widgets/widgets/skeleton_widget.dart';
import 'package:flutter/material.dart';

class AvatarWidget extends StatelessWidget {
  final String imageUrl;
  final String name;
  final double? width;
  final double? height;
  final BoxShape? boxShape;

  const AvatarWidget({
    Key? key,
    required this.imageUrl,
    required this.name,
    this.width,
    this.height,
    this.boxShape,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      decoration: const BoxDecoration(
        shape: BoxShape.circle,
      ),
      child: CachedNetworkImage(
        imageUrl: imageUrl,
        width: width,
        height: height,
        fit: BoxFit.fill,
        placeholder: (context, url) => SkeletonWidget.circular(
          width: width ?? 50,
          height: height ?? 50,
        ),
        errorWidget: (context, url, error) =>
            NoAvatarPlaceholderWidget.colorful(
          name: name.isEmpty ? "Null" : name[0],
          colorize: true,
          fontSize: 14,
        ),
      ),
    );
  }
}
