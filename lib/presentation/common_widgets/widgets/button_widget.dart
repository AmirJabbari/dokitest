import 'package:dokitest/core/utils/extensions/context_entension.dart';
import 'package:dokitest/presentation/common_widgets/widgets/loading_widget.dart';
import 'package:dokitest/presentation/common_widgets/widgets/text_widget.dart';
import 'package:dokitest/presentation/utils/constants/button_styles.dart';
import 'package:flutter/material.dart';

enum ButtonLoadingStatus { loading, normal }

class ButtonWidget extends StatelessWidget {
  final BuildContext context;
  final Color? foregroundColor;
  final Color? loadingColor;
  final bool? isPrimaryCircularLoading;
  final TextStyle? additionalTextStyle;
  final VoidCallback? onPressed;
  final ButtonStyle? style;
  final String? text;
  final double? width;
  final double? height;
  final double? horizontalPadding;
  final double? verticalPadding;
  final bool isEnable;
  final ButtonLoadingStatus? loadingStatus;
  final double? borderRadius;
  final dynamic rightIcon;
  final dynamic leftIcon;
  final Color? iconColor;

  ButtonWidget.fill({
    Key? key,
    required this.context,
    this.foregroundColor,
    this.loadingColor,
    this.additionalTextStyle,
    this.onPressed,
    this.width,
    this.height,
    this.horizontalPadding,
    this.verticalPadding,
    this.iconColor,
    this.text,
    this.isPrimaryCircularLoading,
    this.borderRadius,
    this.isEnable = true,
    this.loadingStatus = ButtonLoadingStatus.normal,
    Color? backgroundColor,
    this.rightIcon,
    this.leftIcon,
  })  : style = ButtonStyles.fill(
          context: context,
          backgroundColor: backgroundColor,
          additionalTextStyle: additionalTextStyle,
          foregroundColor: foregroundColor,
          borderRadius: borderRadius,
        ),
        super(key: key);

  ButtonWidget.outline({
    Key? key,
    required this.context,
    this.foregroundColor,
    this.loadingColor,
    this.iconColor,
    this.additionalTextStyle,
    this.onPressed,
    this.width,
    this.rightIcon,
    this.horizontalPadding,
    this.verticalPadding,
    this.leftIcon,
    this.height,
    this.text,
    this.isPrimaryCircularLoading,
    this.borderRadius,
    this.isEnable = true,
    this.loadingStatus = ButtonLoadingStatus.normal,
    Color? borderColor,
    Color? backgroundColor,
  })  : style = ButtonStyles.outline(
          context: context,
          borderColor: borderColor,
          additionalTextStyle: additionalTextStyle,
          foregroundColor: foregroundColor,
          borderRadius: borderRadius,
        ),
        super(key: key);

  ButtonWidget.text({
    Key? key,
    required this.context,
    this.foregroundColor,
    this.loadingColor,
    this.additionalTextStyle,
    this.onPressed,
    this.width,
    this.height,
    this.rightIcon,
    this.iconColor,
    this.horizontalPadding,
    this.verticalPadding,
    this.leftIcon,
    this.text,
    this.isPrimaryCircularLoading,
    this.borderRadius,
    this.isEnable = true,
    this.loadingStatus = ButtonLoadingStatus.normal,
  })  : style = ButtonStyles.text(
          context: context,
          additionalTextStyle: additionalTextStyle,
          foregroundColor: foregroundColor,
          borderRadius: borderRadius,
        ),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height ?? 48,
      child: ElevatedButton(
        onPressed: isEnable
            ? () {
                if (loadingStatus != ButtonLoadingStatus.loading) {
                  onPressed?.call();
                }
              }
            : null,
        style: style,
        clipBehavior: Clip.antiAlias,
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: horizontalPadding ?? 0,
            vertical: verticalPadding ?? 0,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 20,
                width: loadingStatus == ButtonLoadingStatus.loading ? 20 : 0,
                child: loadingStatus == ButtonLoadingStatus.loading
                    ? Loading(
                        color: loadingColor ?? context.colorScheme.primary,
                      )
                    : const SizedBox.shrink(),
              ),
              if (text != null)
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: TextWidget(
                    text ?? "",
                    context: context,
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
