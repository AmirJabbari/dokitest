import 'package:dokitest/presentation/common_widgets/widgets/appbar_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class BaseView<W extends GetxController> extends GetView<W> {
  final bool includeHorizontalPadding;
  final bool includeVerticalPadding;
  final bool includeFab;
  final bool resizeToAvoidBottomSheet;
  final bool includeTopSafeArea;

  const BaseView({
    Key? key,
    this.includeHorizontalPadding = true,
    this.includeVerticalPadding = true,
    this.resizeToAvoidBottomSheet = true,
    this.includeTopSafeArea = true,
    this.includeFab = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTapScreen,
      child: WillPopScope(
        onWillPop: () async {
          return await onPop.call()?.call() ?? true;
        },
        child: Scaffold(
          backgroundColor: backgroundColor(context),
          drawer: drawer(),
          resizeToAvoidBottomInset: resizeToAvoidBottomSheet,
          appBar: appBar(context),
          bottomNavigationBar: bottomWidget(),
          body: SafeArea(
            top: includeTopSafeArea,
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: includeHorizontalPadding ? 16 : 0,
                  vertical: includeVerticalPadding ? 16 : 0),
              child: body(context),
            ),
          ),
        ),
      ),
    );
  }

  void initialization() => {};

  void onTapScreen() => {};

  void onBuild(BuildContext context) => {};

  Color? backgroundColor(BuildContext context) => null;

  WillPopCallback? onPop() => null;

  Widget? drawer() => null;

  Widget? bottomWidget() => null;

  String? provideRouteName() => null;

  Widget body(BuildContext context);

  AppBarWidget? appBar(BuildContext context) => null;

  Function? onPageReload(BuildContext context) => null;

  void onFabClick(BuildContext context) => () {};
}
