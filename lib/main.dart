import 'package:dokitest/core/dependency/di.dart';
import 'package:dokitest/infrastructure/navigation/navigation.dart';
import 'package:dokitest/infrastructure/theme/theme.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import 'core/utils/localizition/localization_service.dart';


void main() async {
  DependencyCreator.init();
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: "doki test",
      theme: lightTheme,
      locale: LocalizationService.locale,
      debugShowCheckedModeBanner: false,
      fallbackLocale: LocalizationService.fallbackLocale,
      translations: LocalizationService(),
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
      builder: (context, child) {
        return GestureDetector(
          onPanDown: (details) {
            FocusManager.instance.primaryFocus?.unfocus();
          },
          child: child,
        );
      },
    );
  }
}
