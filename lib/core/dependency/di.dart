import 'package:dokitest/data/data_sources/user_data_source/user_data_source_impl.dart';
import 'package:dokitest/data/repositories/auth_repository_impl.dart';
import 'package:dokitest/data/repositories/user_repository_impl.dart';
import 'package:get/get.dart';

class DependencyCreator {
  static init() {
    Get.lazyPut(() => AuthRepositoryIml());
    Get.lazyPut(() => UserDataSourceImpl());
    Get.lazyPut(() =>
        UserRepositoryImpl(userDataSource: Get.find<UserDataSourceImpl>()));
  }
}
