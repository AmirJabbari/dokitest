abstract class BaseRepository {
  Future<ResponseType> safeCall<ResponseType, ResponseModelType>({
    required Future<ResponseModelType> method,
    required ResponseType Function(ResponseModelType) mapper,
    Function(ResponseModelType)? onSuccess,
  }) async {
    final responseModel = await method;
    final response = mapper.call(responseModel);
    await onSuccess?.call(responseModel);
    return response;
  }
}
