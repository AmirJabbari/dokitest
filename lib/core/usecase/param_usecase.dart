abstract class ParamUseCase<Response, Params> {
  Future<Response> call(Params params);
}