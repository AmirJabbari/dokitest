// English Translations
const Map<String, String> faIR = {
  "login_login": "ورود",
  "login_username": "نام کاربری",
  "login_password": "رمز عبور",
  "login_password_error": "رمز عبور باید حداقل 8 کارکتر باشد",
  "login_username_error": "نام کاربی باید حداقل 3 کارکتر باشد",
};