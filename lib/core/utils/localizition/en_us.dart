// English Translations
const Map<String, String> enUS = {
  "login_login": "Login",
  "login_username": "User name",
  "login_password": "password",
  "login_password_error": "رمز عبور باید حداقل 8 کارکتر باشد",
  "login_username_error": "نام کاربی باید حداقل 3 کارکتر باشد",
};