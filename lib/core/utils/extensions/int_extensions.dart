import 'dart:math';

import 'package:intl/intl.dart';

extension IntExtensions on int{

  int generateRandom({int min = 0}) {
    final random = Random();
    return min + random.nextInt(this - min);
  }
}